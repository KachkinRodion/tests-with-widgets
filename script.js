var data = [];
var steps = [];
var scores = [];
var oldData = false;
var currentTask=0;
var oldTask = 0;
var lastElementId = false;
var isNewLevel = false;
var focusElementId = false;
var windowWidth = 0;
setInterval(function(){
    if(data[currentTask]){
		for(prop in data[currentTask])
		//alert(JSON.stringify(data[currentTask]));
			$('#reloadCancel').hide();
    }
}, 1000);
$( document ).ready(redraw);
$(window).resize(resize);

function resize(){
	if(windowWidth != $(window).width()){
		if($(':focus'))
			focusElementId = $(':focus').attr('id');
		var maxPlusOne = data.length>currentTask ? data.length : currentTask+1;
		var tmp = currentTask;
		for(var i=0;i<maxPlusOne;i++){
			currentTask = i;
			redraw('soft');
			if(i>0) $('#content'+i).css('top', (parseInt($('#content'+(currentTask-1)).css('top'))+parseInt($('#content'+(currentTask-1)).css('height')))+'px');
		}
		$('#'+focusElementId).focus();
		currentTask = tmp;
	}
}

function redraw(mode){
    windowWidth = $(window).width();
//getContainer();

    if(mode=='hard'){
        data=[];
        currentTask=0;
        lastElementId = false;
        if(document.getElementById('wrongText')) 
            $('#wrongText').html('');
    }
    //var wrongDiv = document.getElementById('wrongText');
    //if(!wrongDiv) {


    //}
    //setSize($('#wrongText'),'width',450);
    getContainer().html('');
    if(task[currentTask].settings && task[currentTask].settings.skin && task[currentTask].settings.skin=='children'){
        getContainer().addClass('children');
        getContainer().removeClass('adult');
    }else {
        getContainer().removeClass('children');
        getContainer().addClass('adult');
    }
    if(task[currentTask].settings && task[currentTask].settings.background && task[currentTask].settings.background == 'dark-background'){
        getContainer().addClass('dark-background');
        getContainer().removeClass('white-background');
    } else {
        getContainer().addClass('white-background');
        getContainer().removeClass('dark-background');
    }
    getContainer().append($('<div id="wrongText" class="result"></div>'));
    if(task[currentTask].settings && task[currentTask].settings.style)
        getContainer().css(task[currentTask].settings.style);
    setSize($('body'),'font-size',12);
    if($(window).width()/$(window).height()>1.8 && (!task[0].settings || !task[0].settings.autoHeight)){
        var width = getContainer().height()*1.78;
        getContainer().css('width', width+'px');
        getContainer().css('margin-left', '-'+width/2+'px');
    } else {
        getContainer().css('width', '100%');
        getContainer().css('margin-left', '-50%');
    }
    if(task[currentTask].settings && task[currentTask].settings.showSteps){
        redrawSteps();
    }
    if(task[currentTask]){
        var reloadBlock = $('<div id="reloadBlock"></div>');
        var reloadButton = $('<div id="reload-button"></div>');
        var reloadCancel = $('<div id="reloadCancel" class="reload-undo">Вы начали задание заново. <span>Вернуть всё как было</span><img src="close.svg" style="position: absolute; right: .1em; top: .1em; width: 1.5em;"></div>');
        if(!oldData || isNewLevel) reloadCancel.hide();
        setSize(reloadBlock,'top',10);
        setSize(reloadBlock,'right',10);
        setSize(reloadBlock,'width',100);
        //setSize(reloadButton,'width',36);
        //setSize(reloadButton,'margin-left',64);
        //setSize(reloadButton,'height',40);
        setSize(reloadCancel,'font-size',16);
        reloadBlock.append(reloadButton);
        getContainer().append(reloadCancel);
        getContainer().append(reloadBlock);
        reloadButton.click(function(){
             var tmp = JSON.stringify(data);
             oldData = JSON.parse(tmp);
             oldTask = currentTask;
             $(this).parent().remove();
             redraw('hard');
             $('.taskArea').each(function(){
                if(this.id!="content0")
                    $(this).remove();
             });
        });
        reloadCancel.children('span').click(function(){

            var tmp = JSON.stringify(oldData);
            data = JSON.parse(tmp);
            $('#reloadCancel').hide();
            currentTask = oldTask;
          
            setTimeout(function(){$('#reloadCancel').remove();},100);
            redraw('soft');
        });
        reloadCancel.children('img').click(function(){$('#reloadCancel').remove();});
//reloadBtn
        if(currentTask>0){
            if(task[currentTask] && task[currentTask].back && task[currentTask].back.backButton){
                var bbData = task[currentTask].back.backButton;
            } else {
                var bbData = {
                    "type":"light",
                    "text":"Назад",
                    "positions":{"width":156,"height":44,"top":4}
                }
            }
            var backBtn = addButton(bbData);//$('<div id="backButton">Назад</div>');
            backBtn.attr('id', 'backButton');
            backBtn.click(function(){
                $('#content'+currentTask).remove();
                currentTask--;
                redraw('soft');

            });
            //$('#content').append(backBtn);
        }

        for(var key in task[currentTask]){
            if(key=='check'){
                if(!task[currentTask].check.autoCheck){
                    var btn = (task[currentTask].check.checkButton) ? task[currentTask].check.checkButton : {
                    "type":"dark",
                    "icon":"check.png",
                    "text":"Проверить",
                    "positions":{"width":156,"height":44,"top":494,"left":840}
                }; 
                    var checkbutton = addButton(btn);
                    checkbutton.click(function(){check();});
                    $('#wrongText').css('top',checkbutton.css('top'));
                    $('#wrongText').css('height',checkbutton.css('height'));
                    setSize($('#wrongText'),'font-size',20);
                    $('#wrongText').hide();
                }
            } else {
                if(key=='help'){
                    var btn = task[currentTask].help.helpButton ? task[currentTask].help.helpButton : {
                    "type":"light",
                    "icon":"hint.png",
                    "text":"Подсказка",
                    "positions":{"width":156,"height":44,"top":494}
                };  if(!btn.checkbox)
                        var helpbutton = addButton(btn);
                    else {
                        var helpbutton = $('<div></div>');
                        getContainer().append(helpbutton);
                        helpbutton.addClass('button');
                        if(btn.type) 
							helpbutton.addClass(btn.type+'Button');
                        else helpbutton.addClass('lightButton');

                        if(btn.positions){
                            if(btn.positions.width && btn.positions.height){
                                if(btn.positions.width/btn.positions.height<3)
                                    btn.positions.height = btn.positions.width/3;
                            } else 
                                btn.positions = {"width":120,"height":30};
                                for(key in btn.positions)
                                    helpbutton.css(key, getSize(btn.positions[key])+'px');
                                    if(btn.positions.height)
                                        helpbutton.css('border-radius', Math.round(getSize(btn.positions.height*0.1))+'px');
                       }
                       var text=$('<div class="buttonText"><input class="magic-checkbox" type="checkbox" id="helpCheckbox"><label for="helpCheckbox">'+btn.text+'</label></div>');
					   text.find('label').click(function(){
						   //alert($(this).parent().find('input:eq(0)').attr('id'));
						   $(this).parent().find('input:eq(0)').attr('checked',true);;
					   });
                       text.css('font-size', getSize(btn.positions.height*0.4)+'px');
                       text.find("input").css("width", getSize(btn.positions.height*0.4)+'px');
                       text.find("input").css("height", getSize(btn.positions.height*0.4)+'px');
                       text.css('top', getSize(btn.positions.height*0.20)+'px');
                       text.css('width', '100%');
                       helpbutton.append(text);
                    }
                    helpbutton.attr('id','helpButton'+currentTask);
                    setInterval(function(){
                        if(window.help && !window.help.enabled)
                            $('#helpButton'+currentTask).find('input').each(function(){$(this).removeAttr('checked');});
                    },2000);
                    helpbutton.find('input').click(function(e){
                        //e.preventDefault();
                        $('#helpButton'+currentTask).trigger( "click" );
                    
                    });
                    helpbutton.click(function(){
                        if(window.help)
                            if(window.help.enabled){
                                $(this).find('input').each(function(){$(this).removeAttr('checked');})
                                if(help[task[currentTask].help.type].stop)
                                    help[task[currentTask].help.type].stop();
                                window.help.enabled = false;
                            } else {
                                $(this).find("input").each(function(){this.checked='checked';});
                                if(help[task[currentTask].help.type].start)
                                    help[task[currentTask].help.type].start();
                                window.help.enabled = true;
                            }
                    });

                    if(task[currentTask].help.type && window.help && window.help[task[currentTask].help.type]){
                        var currentHelp = help[task[currentTask].help.type];
                        if(currentHelp.init)
                            currentHelp.init();
                    }
                } else {
                    if(window[key])
                        window[key].create(task[currentTask][key]).each(
                           function(){
                               getContainer().append($(this));
                           }
                       );
                        
                }
            }

        }
    }
}

function redrawSteps(num){
    num = num || currentTask;
    var stepDiv = document.getElementById('steps');
    if(stepDiv)
        var element = $(stepDiv);//<div class="steps"></div>
    else{
        var element = $('<div id="steps"></div>');
        $('body').prepend(element);
    }
    element.html('');
    var style='width:'+getSize(30)+'px;height:'+getSize(30)+'px;border-radius:'+getSize(15)+'px;font-size:'+getSize(20)+'px;';
    for(var i=0;i<task.length;i++){
        if(num>i){
            element.append($('<div class="'+steps[i]+'" style="'+style+'">'+(i+1)+'</div>'));
        } else 
            if(i==num)
                element.append($('<div class="currentStep" style="'+style+'">'+(i+1)+'</div>'));
            else
                element.append($('<div class="newStep" style="'+style+'">'+(i+1)+'</div>'));
    }
}
function showCheckAnimation(){
    //if(task[currentTask] && task[currentTask].settings && task[currentTask].settings.wrongText)
    //    return false;
    return true;
}
function check(){
	//alert(JSON.stringify(data[currentTask]));
    var result = true;
    var currentScore=0;
    for(var key in task[currentTask]){
       if(key!='check' && key!='help' && key!='settings' && window[key] && window[key].check){

          var tmpresult = window[key].check();
//alert(key+' '+JSON.stringify(tmpresult));
          currentScore+=tmpresult.score;
          if(!tmpresult.result)
            result = false;
          
       }
    }

    if(task[currentTask] && task[currentTask].settings && task[currentTask].settings.wrongText){
        $('#wrongText').show();
        if(result){
            $('#wrongText').removeClass('bad');
            $('#wrongText').addClass('good');
			if(task[currentTask].settings.rightText)
				$('#wrongText').html(task[currentTask].settings.rightText);
			else
				$('#wrongText').html('Правильно');

  
        } else{
            $('#wrongText').html(task[currentTask].settings.wrongText);
            $('#wrongText').addClass('bad');
            $('#wrongText').removeClass('good');
			setTimeout(function(){$('#wrongText').hide();},1000);
        }
    }

    if(result || (task[currentTask].settings && task[currentTask].settings.mode=='hard')){
        scores[currentTask] = currentScore;
        steps[currentTask] = result ? 'rightStep':'wrongStep';
        for(var key in task[currentTask]){
            if(window[key] && window[key].stageSuccess){
                window[key].stageSuccess();
            }
        }

        if(!task[currentTask+1]){
            var totalScore=0;
            for(var i=0;i<scores.length;i++){
                totalScore+=scores[i];
                
            }
            if(totalScore>0 && task[currentTask].settings && task[currentTask].settings.totalScoreText){
                $('#wrongText').html( task[currentTask].settings.totalScoreText.prefix+' '+totalScore+' '+task[currentTask].settings.totalScoreText.postfix)
            }
            check_.startAnimation();
            if(task[currentTask].settings && task[currentTask].settings.showSteps){
                redrawSteps(currentTask+1);
            }
        } else {
            $('#reloadBlock').remove();
            //if(!data[currentTask]+1) data[currentTask+1] = {};
            isNewLevel = true;
            $('#content'+currentTask).children().each(function(){
                this.removeAttribute('id');
                //$(this).css('opacity',0.8);
                $(this).draggable('destroy');
            });
            check_.startAnimation();
            currentTask++;
            setTimeout(function(){redraw('soft');isNewLevel=false;}, 3000);
        }
    }
}

function getSize(a){
    return Math.round($('#content0').width()/1000*a);
}
function setSize(obj,key,value,nozero){
	if(nozero && getSize(value)==0)
		obj.css(key, '1px');
	else
		obj.css(key, getSize(value)+'px');
}
function addButton(widget){
    var button = $('<div></div>');
    if(widget.type){
         var className = widget.type=='dark' ?'check-button' : 'button';
	     button.addClass(widget.type+'Button');
    } else className = 'button';
        button.addClass(className);
    if(widget.positions){
        if(widget.positions.width && widget.positions.height){
            if(widget.positions.width/widget.positions.height<3)
                widget.positions.height = widget.positions.width/3;
        } else 
            widget.positions = {"width":120,"height":30};
        for(key in widget.positions)
            button.css(key, getSize(widget.positions[key])+'px');
            if(widget.positions.height)
                button.css('border-radius', Math.round(getSize(widget.positions.height*0.1))+'px');
    }
    var text=$('<div class="buttonText">'+widget.text+'</div>');
    text.css('font-size', getSize(widget.positions.height*0.4)+'px');
    //text.css('top', getSize(widget.positions.height*0.20)+'px');
    //text.css('left', getSize(widget.positions.width*0.5)+'px');
    if(widget.icon && widget.positions && widget.positions.height){
        text.css('padding-left', getSize(widget.positions.height*0.6)+'px');
        text.css('background-image', 'url('+widget.icon+')');
    }
    var tmpDiv = $('<div class="innerButtonDiv"></div>');
    tmpDiv.css('width',button.css('width'));
    tmpDiv.css('height',button.css('height'));
    tmpDiv.append(text);
    button.append(tmpDiv);
    getContainer().append(button);
    //text.css('margin-left', '-'+Math.round(text.outerWidth()/2)+'px');
    return button;
}

function getContainer(){
    if(task[0].settings && task[0].settings.oneScreen ){
        if(document.getElementById('content'+currentTask)) 
            return $('#content'+currentTask);
        else {
            var container = $('<div id="content'+currentTask+'" class="taskArea"></div>');
            container.css('top', (parseInt($('#content'+(currentTask-1)).css('top'))+parseInt($('#content'+(currentTask-1)).css('height')))+'px');
            //container.data('top', Math.round($('#content0').width()*1000/$('#content'+currentTask).offset().top));
            $('body').append(container);
            window.scrollTo(0,parseInt($('#content'+currentTask).css('top')));
            return container;
        }
    } else {
        return $('#content0')
    }
}

/*! modernizr 3.3.1 (Custom Build - brderimage) | MIT *
 * https://modernizr.com/download/?-borderimage-setclasses !*/
!function (e, n, t) { function r(e, n) { return typeof e === n } function o() { var e, n, t, o, s, i, a; for (var l in C) if (C.hasOwnProperty(l)) { if (e = [], n = C[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (t = 0; t < n.options.aliases.length; t++) e.push(n.options.aliases[t].toLowerCase()); for (o = r(n.fn, "function") ? n.fn() : n.fn, s = 0; s < e.length; s++) i = e[s], a = i.split("."), 1 === a.length ? Modernizr[a[0]] = o : (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = o), g.push((o ? "" : "no-") + a.join("-")) } } function s(e) { var n = _.className, t = Modernizr._config.classPrefix || ""; if (S && (n = n.baseVal), Modernizr._config.enableJSClass) { var r = new RegExp("(^|\\s)" + t + "no-js(\\s|$)"); n = n.replace(r, "$1" + t + "js$2") } Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), S ? _.className.baseVal = n : _.className = n) } function i(e, n) { return !!~("" + e).indexOf(n) } function a() { return "function" != typeof n.createElement ? n.createElement(arguments[0]) : S ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments) } function l(e) { return e.replace(/([a-z])-([a-z])/g, function (e, n, t) { return n + t.toUpperCase() }).replace(/^-/, "") } function f(e, n) { return function () { return e.apply(n, arguments) } } function u(e, n, t) { var o; for (var s in e) if (e[s] in n) return t === !1 ? e[s] : (o = n[e[s]], r(o, "function") ? f(o, t || n) : o); return !1 } function d(e) { return e.replace(/([A-Z])/g, function (e, n) { return "-" + n.toLowerCase() }).replace(/^ms-/, "-ms-") } function p() { var e = n.body; return e || (e = a(S ? "svg" : "body"), e.fake = !0), e } function c(e, t, r, o) { var s, i, l, f, u = "modernizr", d = a("div"), c = p(); if (parseInt(r, 10)) for (; r--;) l = a("div"), l.id = o ? o[r] : u + (r + 1), d.appendChild(l); return s = a("style"), s.type = "text/css", s.id = "s" + u, (c.fake ? c : d).appendChild(s), c.appendChild(d), s.styleSheet ? s.styleSheet.cssText = e : s.appendChild(n.createTextNode(e)), d.id = u, c.fake && (c.style.background = "", c.style.overflow = "hidden", f = _.style.overflow, _.style.overflow = "hidden", _.appendChild(c)), i = t(d, e), c.fake ? (c.parentNode.removeChild(c), _.style.overflow = f, _.offsetHeight) : d.parentNode.removeChild(d), !!i } function m(n, r) { var o = n.length; if ("CSS" in e && "supports" in e.CSS) { for (; o--;) if (e.CSS.supports(d(n[o]), r)) return !0; return !1 } if ("CSSSupportsRule" in e) { for (var s = []; o--;) s.push("(" + d(n[o]) + ":" + r + ")"); return s = s.join(" or "), c("@supports (" + s + ") { #modernizr { position: absolute; } }", function (e) { return "absolute" == getComputedStyle(e, null).position }) } return t } function h(e, n, o, s) { function f() { d && (delete z.style, delete z.modElem) } if (s = r(s, "undefined") ? !1 : s, !r(o, "undefined")) { var u = m(e, o); if (!r(u, "undefined")) return u } for (var d, p, c, h, v, y = ["modernizr", "tspan", "samp"]; !z.style && y.length;) d = !0, z.modElem = a(y.shift()), z.style = z.modElem.style; for (c = e.length, p = 0; c > p; p++) if (h = e[p], v = z.style[h], i(h, "-") && (h = l(h)), z.style[h] !== t) { if (s || r(o, "undefined")) return f(), "pfx" == n ? h : !0; try { z.style[h] = o } catch (g) { } if (z.style[h] != v) return f(), "pfx" == n ? h : !0 } return f(), !1 } function v(e, n, t, o, s) { var i = e.charAt(0).toUpperCase() + e.slice(1), a = (e + " " + x.join(i + " ") + i).split(" "); return r(n, "string") || r(n, "undefined") ? h(a, n, o, s) : (a = (e + " " + E.join(i + " ") + i).split(" "), u(a, n, t)) } function y(e, n, r) { return v(e, t, t, n, r) } var g = [], C = [], w = { _version: "3.3.1", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function (e, n) { var t = this; setTimeout(function () { n(t[e]) }, 0) }, addTest: function (e, n, t) { C.push({ name: e, fn: n, options: t }) }, addAsyncTest: function (e) { C.push({ name: null, fn: e }) } }, Modernizr = function () { }; Modernizr.prototype = w, Modernizr = new Modernizr; var _ = n.documentElement, S = "svg" === _.nodeName.toLowerCase(), b = "Moz O ms Webkit", x = w._config.usePrefixes ? b.split(" ") : []; w._cssomPrefixes = x; var E = w._config.usePrefixes ? b.toLowerCase().split(" ") : []; w._domPrefixes = E; var P = { elem: a("modernizr") }; Modernizr._q.push(function () { delete P.elem }); var z = { style: P.elem.style }; Modernizr._q.unshift(function () { delete z.style }), w.testAllProps = v, w.testAllProps = y, Modernizr.addTest("borderimage", y("borderImage", "url() 1", !0)), o(), s(g), delete w.addTest, delete w.addAsyncTest; for (var N = 0; N < Modernizr._q.length; N++) Modernizr._q[N](); e.Modernizr = Modernizr }(window, document);