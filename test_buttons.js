var test_buttons ={
    create:function(input){
        var result = [];
        var buttonsPopup = $('<div id="popup_test_button'+i+'" class="buttonsPopup" style="display:none"></div>');
        
        for(var i=0;i<input.length;i++){
            var button = addButton(input[i].button);
            button.attr('id','test_button'+i);
			button.data('num', i)
			button.click(function(){
				$('#buttonPopup'+$(this).data('num')).show();
				
			});
            result.push(button);
			var buttonPopup = $('<div class="popup buttonPopup" id="buttonPopup'+i+'" style="display:none"></div>');
			if(input[i].popup)
				if(input[i].popup.text)
					buttonPopup.html(input[i].popup.text);
				if(input[i].popup.style)
					buttonPopup.css(input[i].popup.style);
				if(input[i].popup.positions)
					for(var key in input[i].popup.positions)
						setSize(buttonPopup, key, input[i].popup.positions['key']);
			buttonPopup.click(function(){$(this).hide();});
			result.push(buttonPopup);
        }
        return $(result);
    }
}