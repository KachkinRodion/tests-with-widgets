var test_drag_n_drop = {
    create:function(input){
		if(!$.isArray(input))
			input = [input];
		var result = [];
		for(var blockNum=0;blockNum<input.length;blockNum++){

			if(input[blockNum].answers)

			for(var i=0;i<input[blockNum].answers.length;i++){
				this.defaultplaces['block'+blockNum+'defaultPlace'+i] = 'block'+blockNum+"draggable"+i;
				var answer = $('<div id="block'+blockNum+'draggable'+i+'" class="draggable"></div>');
				var defaultplace = $('<div id="block'+blockNum+'defaultPlace'+i+'" class="defaultPlace"></div>');
				if(input[blockNum].answerParams.style){
					answer.css(input[blockNum].answerParams.style);
				}
				if(input[blockNum].answers[i].style){
					answer.css(input[blockNum].answers[i].style);
				}
				if(input[blockNum].answers[i].hint)
					answer.data('hint',input[blockNum].answers[i].hint);
				if(input[blockNum].answerParams.positions){
					for(key in input[blockNum].answerParams.positions){
						setSize(answer,key,input[blockNum].answerParams.positions[key]);
						setSize(defaultplace,key,input[blockNum].answerParams.positions[key]);
					}
				}

				if(input[blockNum].answers[i].positions){
					for(key in input[blockNum].answers[i].positions){
						setSize(answer,key,input[blockNum].answers[i].positions[key]);
						setSize(defaultplace,key,input[blockNum].answers[i].positions[key]);
					}
				}
				if(input[blockNum].answers[i].onStopDragging){//leave, defaultplace
					answer.data('stopdragging',input[blockNum].answers[i].onStopDragging);
				} else {
					if(input[blockNum].answerParams.onStopDragging){
						answer.data('stopdragging',input[blockNum].answerParams.onStopDragging);
					} else {
						answer.data('stopdragging','leave');
					}
				}
				var element = $('<div>'+input[blockNum].answers[i].value+'</div>');
				element.css('width',answer.css('width'));
				element.css('height',answer.css('height'));
				if(input[blockNum].answerParams.textOffset)
					element.css('top',input[blockNum].answerParams.textOffset+'px');
				if(input[blockNum].answerParams.textShow && input[blockNum].answerParams.textShow=="removeOnTarget"){
					element.addClass('answerText1');
				}
				if(input[blockNum].answerParams.innerStyle){
					element.css(input[blockNum].answerParams.innerStyle);
				}
				if(input[blockNum].answers[i].innerStyle){
					element.css(input[blockNum].answers[i].innerStyle);
				}
				if(input[blockNum].answerParams.innerPositions){
					for(var key in input[blockNum].answerParams.innerPositions){
						setSize(element,key,input[blockNum].answerParams.innerPositions[key]);
					}
				}
				if(input[blockNum].answers[i].innerPositions){
					for(var key in input[blockNum].answers[i].innerPositions){
						setSize(element,key,input[blockNum].answers[i].innerPositions[key]);
					}
				}
				answer.append(element);
				defaultplace.droppable({
					out: test_drag_n_drop.defaultPlaceHandleOutEvent,
					drop: test_drag_n_drop.defaultPlaceHandleDropEvent
				});

				answer.mousedown(function(){$(this).addClass('active');});
				if(input[blockNum].answers[i].renewable){
				answer.mouseup(function(){
					$(this).removeClass('active');
					setTimeout(function(a){
						if(!a.hasClass('in-the-droppable') && test_drag_n_drop.defaultplaces["defaultPlace"+test_drag_n_drop.numberFromId(a[0].id)]!=a[0].id){
								a.remove();
                        }
                    },100, $(this));

                });
				} else
					answer.mouseup(function(){$(this).removeClass('active');test_drag_n_drop.checkInDroppable();});
				result.push(defaultplace);
				result.push(answer);
				setTimeout(function(a){
					a.draggable({stop:test_drag_n_drop.handleStopDraggingEvent});
				}, 100,answer);
			}
			if(input[blockNum].correctAnswers)
				for(var i=0;i<input[blockNum].correctAnswers.length;i++){
					var droppable = $('<div id="block'+blockNum+'droppable'+i+'" class="droppable"></div>');
					if(input[blockNum].correctAnswers[i].hint)
						droppable.data('hint',input[blockNum].correctAnswers[i].hint);
					if(input[blockNum].correctAnswerParams && input[blockNum].correctAnswerParams.style)
						droppable.css(input[blockNum].correctAnswerParams.style);
					if(input[blockNum].correctAnswers[i].style)
						droppable.css(input[blockNum].correctAnswers[i].style);
					if(input[blockNum].correctAnswerParams.positions)
						for(key in input[blockNum].answerParams.positions){
							setSize(droppable,key,input[blockNum].correctAnswerParams.positions[key]);
						}
					if(input[blockNum].correctAnswers[i].positions)
						for(key in input[blockNum].correctAnswers[i].positions){
							setSize(droppable,key,input[blockNum].correctAnswers[i].positions[key]);
					}
					droppable.droppable({
						//tolerance:"pointer",
						drop: test_drag_n_drop.handleDropEvent,
						out: test_drag_n_drop.handleOutEvent
					});

					result.push(droppable);
				}
			setTimeout(function(a,b){
				for(var i=0;i<a.length;i++)
					if(data[currentTask] && data[currentTask]['block'+b+'droppable'+i]){
						$('#'+data[currentTask]['block'+b+'droppable'+i]).css('top', $('#block'+b+'droppable'+i).css('top'));
						$('#'+data[currentTask]['block'+b+'droppable'+i]).css('left', $('#block'+b+'droppable'+i).css('left'));
					}
			},100,input[blockNum].correctAnswers,blockNum);
		}
		return $(result);
	},
    check:function(){

		var result = true;
		var input1 = task[currentTask]['test_drag_n_drop'];
		if(!$.isArray(input1))
			input1 = [input1];
		for(var blockNum=0;blockNum<input1.length;blockNum++){
			var dnd = input1[blockNum];
			if(dnd && dnd.correctAnswers){
				var input = dnd.correctAnswers;
				var matrixCounter = [];
				var matrixTmp = [];
				var firstMatrixElement = true;
				for(var i=0;i<input.length;i++){
					//alert(JSON.stringify(data[currentTask]['block'+blockNum+'droppable'+i]+"\n"+'block'+blockNum+'droppable'+i));

					if(data[currentTask] && data[currentTask]['block'+blockNum+'droppable'+i]){
						var tmpresult = false;
						
						if(dnd.correctAnswers[i].values || dnd.correctAnswers[i].values==0){
							if(typeof dnd.correctAnswers[i].values =='string'){
								var groups = dnd.correctAnswers[i].values.split('|');
								for(var k=0;k<groups.length;k++){
									var group = dnd.correctAnswersGroups[groups[k]] || [];
									for(var j=0;j<group.length;j++){
									//alert(dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value);
										if(group[j]==dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value && 
											(!test_drag_n_drop.groups[dnd.correctAnswers[i].values] 
												|| !test_drag_n_drop.groups[dnd.correctAnswers[i].values][group[j]] 
												|| test_drag_n_drop.groups[dnd.correctAnswers[i].values][group[j]]==i) 
											&& test_drag_n_drop.checkSupergroupByAnswerId(i,blockNum)/**/){
											tmpresult=true;
										}
									}
								}
							} else {
								if(typeof dnd.correctAnswers[i].values == "number"){
									if(firstMatrixElement){
										if(!matrixCounter[dnd.correctAnswers[i].values])
											matrixCounter[dnd.correctAnswers[i].values] = 0;
										for(var j=0;j<dnd.matrixSets[dnd.correctAnswers[i].values].length;j++){
											if(dnd.matrixSets[dnd.correctAnswers[i].values][j][matrixCounter[dnd.correctAnswers[i].values]]==dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value){
											//alert(dnd.matrixSets[]);
												if(!matrixTmp[dnd.correctAnswers[i].values])
													matrixTmp[dnd.correctAnswers[i].values]={};
										//	alert(JSON.stringify(matrixTmp)+"\n"+i+"\n"+matrixCounter[dnd.correctAnswers[i].values]);
												matrixTmp[dnd.correctAnswers[i].values][j] = true;
												tmpresult = true;
												firstMatrixElement = false;
											}
										}
										matrixCounter[dnd.correctAnswers[i].values]++;
									
									} else {
										for(var j=0;j<dnd.matrixSets[dnd.correctAnswers[i].values].length;j++){
											if(dnd.matrixSets[dnd.correctAnswers[i].values][j][matrixCounter[dnd.correctAnswers[i].values]]==dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value && matrixTmp[dnd.correctAnswers[i].values] && matrixTmp[dnd.correctAnswers[i].values][j]){
											//matrixTmp[dnd.correctAnswers[i].values][j] = false;
												tmpresult = true;
											} else {//alert(j+"\n"+"\n"+matrixCounter[dnd.correctAnswers[i].values]+"\n"+dnd.matrixSets[dnd.correctAnswers[i].values][j][matrixCounter[dnd.correctAnswers[i].values]]+'=='+dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value);
												if(!matrixTmp[dnd.correctAnswers[i].values])
													matrixTmp[dnd.correctAnswers[i].values]={};
												matrixTmp[dnd.correctAnswers[i].values][j] = false;
											}
										}
										matrixCounter[dnd.correctAnswers[i].values]++;
									}
								
						//		alert(JSON.stringify(matrixTmp)+"\n"+i);
								} else
									for(var j=0; j<dnd.correctAnswers[i].values.length;j++){
										if(dnd.correctAnswers[i].values[j]==dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value){
										tmpresult=true;
									}
								}
							}
						} else {
							tmpresult = false;
						}
						var thisDroppable = $('#'+data[currentTask]['block'+blockNum+'droppable'+i]);
						var oldStyle = {};
	
						if(thisDroppable[0].style.backgroundColor )
							oldStyle['background-color']=thisDroppable[0].style.backgroundColor;
						else
							oldStyle['background-color']='';
						if(thisDroppable[0].style.backgroundImage)
							oldStyle["background-image"]=thisDroppable[0].style.backgroundImage;
						else
							oldStyle['background-image']='';
					
					
						setTimeout(function(a,b,d){
							a.css(b);
							test_drag_n_drop.isCustomAnimation = false;
						},500,thisDroppable,oldStyle);
						if(tmpresult){
							var style = {};
							if(dnd.answers[test_drag_n_drop.numberFromId(thisDroppable[0].id)].onCorrectAnimation){
								var oncorrectanimation = dnd.answers[test_drag_n_drop.numberFromId(thisDroppable[0].id)].onCorrectAnimation;
							} else {
								if(dnd.onCorrectAnimation)
									var oncorrectanimation = dnd.onCorrectAnimation;
								else 
									var oncorrectanimation = false;
							}
							if((!lastElementId || lastElementId==thisDroppable[0].id) &&oncorrectanimation && oncorrectanimation.type){
								if(oncorrectanimation.type=='color' && oncorrectanimation.value)
									style["background-color"]=oncorrectanimation.value;
								if(oncorrectanimation.type=='image' && oncorrectanimation.value)
									thisDroppable[0].style.backgroundImage='url'+oncorrectanimation.value+')';
								if(oncorrectanimation.type=='rocking')
									thisDroppable.addClass("rocked");
								setTimeout(function(a){
									a.removeClass("rocked");
								},200,thisDroppable);
								if(oncorrectanimation.type=='showhide'){
									if(oncorrectanimation.value.show)
										for(var k=0;k<oncorrectanimation.value.show.length;k++)
											$('#'+oncorrectanimation.value.show[k]).show();
									if(oncorrectanimation.value.hide)
										for(var k=0;k<oncorrectanimation.value.hide.length;k++)
											$('#'+oncorrectanimation.value.hide[k]).hide();
								}
						}
//alert(JSON.stringify(style)+' '+thisDroppable);
					
						if(showCheckAnimation()) {
							$(thisDroppable).css(style);
							thisDroppable.addClass('draggable-right');						
						}
						//setTimeout(function(a,b){a.animate(b);},2500,thisDroppable,style);
						//$('#image0').animate({"border":"green 5px solid"});
						//thisDroppable.animate({"background-color":"red"}/*style*/);
						//alert(thisDroppable);

                    //$('#'+data[currentTask]['droppable'+i]).draggable('destroy');
						} else {
					  	
							var style = {};
							if(dnd.answers[test_drag_n_drop.numberFromId(thisDroppable[0].id)].onWrongAnimation){
								var onwronganimation = dnd.answers[test_drag_n_drop.numberFromId(thisDroppable[0].id)].onWrongAnimation;
							} else {
								if(dnd.onWrongAnimation)
									var onwronganimation = dnd.onWrongAnimation;
								else
									var onwronganimation = false;
							}
							if((!lastElementId || lastElementId==thisDroppable[0].id) &&onwronganimation && onwronganimation.type){
								if(onwronganimation.type=='color' && onwronganimation.value)
									style["background-color"]=dnd.onWrongAnimation.value;
								if(onwronganimation.type=='image' && onwronganimation.value)
									thisDroppable[0].style.backgroundImage='url('+onwronganimation.value+')';
								if(onwronganimation.type=='rocking')
									thisDroppable.addClass("rocked");
								setTimeout(function(a){a.removeClass("rocked");},200,thisDroppable);
								if(onwronganimation.type=='showhide'){
								if(onwronganimation.value.show)
									for(var k=0;k<onwronganimation.value.show.length;k++)
										$('#'+onwronganimation.value.show[k]).show();
								if(onwronganimation.value.hide)
									for(var k=0;k<onwronganimation.value.hide.length;k++)
										$('#'+onwronganimation.value.hide[k]).hide();
								}
								if(onwronganimation.type=='returnToDefaultPlace'){
									var positions = (dnd.answerParams && dnd.answerParams.positions) ? task[currentTask]['test_drag_n_drop'].answerParams : {};
									for(var key in positions){
										setSize(thisDroppable,key,positions[key]);
									}
									positions = dnd.answers[test_drag_n_drop.numberFromId(thisDroppable[0].id)].positions;
									for(var key in positions){
										setSize(thisDroppable,key,positions[key]);
									}
									data[currentTask]['block'+blockNum+'droppable'+i] = false;
									$('#'+'block'+blockNum+'droppable'+i).removeClass('withDraggable');
							   
									setTimeout(function(a){a.removeClass('in-the-droppable');a.removeClass('draggable-wrong');},500,thisDroppable);
								}
							}/**/

							if(showCheckAnimation()) {
								$(thisDroppable).css(style);
								thisDroppable.addClass('draggable-wrong');
							}
							result = false;


						}

					} else {
						//have not draggable in droppable
						if(typeof dnd.correctAnswers[i].values == "number"){
							//alert("nodata\n"+matrixCounter[dnd.correctAnswers[i].values]);
							if(!matrixCounter[dnd.correctAnswers[i].values]){
								matrixCounter[dnd.correctAnswers[i].values] = 0;
							}
							
							var tmpresult = false;
							for(var j=0;j<dnd.matrixSets[dnd.correctAnswers[i].values].length;j++){
								
								if(dnd.matrixSets[dnd.correctAnswers[i].values][j][matrixCounter[dnd.correctAnswers[i].values]]==false){
									if(!matrixTmp[dnd.correctAnswers[i].values])
										tmpresult = true;
									else
										if(matrixTmp[dnd.correctAnswers[i].values][j])
											tmpresult=true;
									/*		//alert(dnd.matrixSets[]);
									if(!matrixTmp[dnd.correctAnswers[i].values])
										matrixTmp[dnd.correctAnswers[i].values]={};
										//	alert(JSON.stringify(matrixTmp)+"\n"+i+"\n"+matrixCounter[dnd.correctAnswers[i].values]);
									//matrixTmp[dnd.correctAnswers[i].values][j] = true;
									tmpresult = true;
									//firstMatrixElement = false;*/
								}
							} 
							matrixCounter[dnd.correctAnswers[i].values]++;
							if(!tmpresult) result = false;
							//alert("nodata2\n"+matrixCounter[dnd.correctAnswers[i].values]);
						} else {
							if(dnd.correctAnswers[i].values)
								result = false;
						}
						
					}
				}
			}
		}

        return {"result":result,"score":0};;
    },
	isCustomAnimation:false,
    defaultplaces:{},
    defaultPlaceHandleOutEvent:function(event,ui){
        var draggable = ui.draggable;
		var input = task[currentTask]['test_drag_n_drop'];
		if(!$.isArray(input))
			input = [input];
		var dnd = input[test_drag_n_drop.numberFromId(event.target.id,true)];
		
//alert(test_drag_n_drop.defaultplaces[event.target.id]+' '+draggable.attr('id'));
        if(test_drag_n_drop.defaultplaces[event.target.id]==draggable.attr('id')){
            test_drag_n_drop.defaultplaces[event.target.id] = false;
	//alert(dnd.answers);
            if(dnd && dnd.answers[test_drag_n_drop.numberFromId(event.target.id)].renewable && dnd.answers[test_drag_n_drop.numberFromId(event.target.id)].renewable==true)
               test_drag_n_drop.addDraggableDuplicate(test_drag_n_drop.numberFromId(event.target.id),test_drag_n_drop.numberFromId(event.target.id,true));
            
        }
    },
    defaultPlaceHandleDropEvent:function(event,ui){
        if(!test_drag_n_drop.defaultplaces[event.target.id]){
            var draggable = ui.draggable;
            draggable.css('left', $(event.target).css('left'));
            draggable.css('top', $(event.target).css('top'));
            test_drag_n_drop.defaultplaces[event.target.id]=draggable.attr('id');
        }
    },
    checkInDroppable:function(){},
    numberFromId:function (id,block){
		var arr = id.split('defaultPlace');//table'+i+'defaultPlace
		if(arr.length==1)
			var arr = id.split('draggable');
		if(arr.length==1)
			var arr = id.split('droppable');
			
				
		if(block){
			var string = arr[0].replace('block','');
		} else {
			var string = arr[1].split('duplicate')[0];
		}
        /*string = string.replace('draggable', '');
        string = string.replace('droppable', '');
        string = string.replace('defaultPlace', '');
        string = string.split('duplicate')[0];*/
        return parseInt(string);
    },
    groups:{},
    removeFromGroup:function(draggable,droppable){
		input = task[currentTask]['test_drag_n_drop'];
		if(!$.isArray(input))
			input = [input];
		var dnd = input[test_drag_n_drop.numberFromId(droppable.attr('id'),true)];
        if(typeof dnd.correctAnswers[test_drag_n_drop.numberFromId(droppable.attr('id'))].values =='string'){
            var groupName = dnd.correctAnswers[test_drag_n_drop.numberFromId(droppable.attr('id'))].values;
            groupName = groupName.split('|');
			for(var i=0;i<groupName.length;i++){
				values = dnd.correctAnswersGroups[groupName[i]];
				for(key in values){
					if(values[key]==dnd.answers[test_drag_n_drop.numberFromId(draggable.attr('id'))].value){
						if(!test_drag_n_drop.groups[groupName[i]])
							test_drag_n_drop.groups[groupName[i]] = {};
						test_drag_n_drop.groups[groupName[i]][values[key]]=false;
					}
				}
			}
        }
    },
	
    addToGroup:function(draggable,droppable){
		input = task[currentTask]['test_drag_n_drop'];
		if(!$.isArray(input))
			input = [input];
		var dnd = input[test_drag_n_drop.numberFromId(droppable.attr('id'),true)];
        if(dnd && typeof dnd.correctAnswers[test_drag_n_drop.numberFromId(droppable.attr('id'))].values =='string'){
            var groupName = dnd.correctAnswers[test_drag_n_drop.numberFromId(droppable.attr('id'))].values;
			groupName = groupName.split('|');
			for(var i=0;i<groupName.length;i++){
				values = dnd.correctAnswersGroups[groupName[i]];
				for(key in values){
					if(values[key]==dnd.answers[test_drag_n_drop.numberFromId(draggable.attr('id'))].value){
						if(!test_drag_n_drop.groups[groupName[i]])
							test_drag_n_drop.groups[groupName[i]] = {};
						test_drag_n_drop.groups[groupName[i]][values[key]]=test_drag_n_drop.numberFromId(droppable.attr('id'));
					}
				}
			}
		}
		
    },
    handleDropEvent:function(event,ui){
        var draggable = ui.draggable;
		
        if((!data[currentTask] || !data[currentTask][event.target.id]) && test_drag_n_drop.numberFromId(event.target.id,true)==test_drag_n_drop.numberFromId(draggable[0].id,true)){
            draggable.css('left', $(event.target).css('left'));
            draggable.css('top', $(event.target).css('top'));
            draggable.removeClass('shadow');
	        draggable.addClass('in-the-droppable');
	        $(event.target).addClass('withDraggable');
	        $(event.target).removeClass('emptyWrong');
            if(!data[currentTask]) data[currentTask]={};
            data[currentTask][event.target.id]=draggable.attr('id');
            test_drag_n_drop.addToGroup(draggable,$(event.target));
            if(task[currentTask].check.autoCheck){
                lastElementId = draggable.attr('id');
                check();
            }
        } else {
			if(test_drag_n_drop.numberFromId(event.target.id,true)==test_drag_n_drop.numberFromId(draggable[0].id,true)){
				draggable.addClass('in-the-droppable');

				test_drag_n_drop.addToGroup(draggable,$(event.target));

            //alert(data[currentTask][event.target.id]);
				var secondDraggable = $('#'+data[currentTask][event.target.id]);
				secondDraggable.removeClass('in-the-droppable');
				if(secondDraggable.data('stopdragging')=='defaultplace')
					secondDraggable.animate({"top":$('#block'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id],true)+'defaultPlace'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id])).css('top'),"left":$('#block'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id],true)+'defaultPlace'+test_drag_n_drop.numberFromId(data[currentTask][event.target.id])).css('left')});
				else
					secondDraggable.css({"left":draggable.css('left'),"top":draggable.css('top')});

				test_drag_n_drop.removeFromGroup(secondDraggable,$(event.target));
				data[currentTask][event.target.id]=draggable.attr('id');
				draggable.css('left', $(event.target).css('left'));
				draggable.css('top', $(event.target).css('top'));

				if(task[currentTask].check.autoCheck){
					lastElementId =  draggable.attr('id');
					check();
				}
			}
            /*draggable.animate({'left': $('#defaultPlace'+test_drag_n_drop.numberFromId(draggable.attr('id'))).css('left')},'fast');
            draggable.animate({'top': $('#defaultPlace'+test_drag_n_drop.numberFromId(draggable.attr('id'))).css('top')},'fast');*/
        }
    },
    handleOutEvent:function(event,ui){
        var draggable = ui.draggable;
    //draggable.css('background','');
        if(data[currentTask] && data[currentTask][event.target.id]==draggable.attr('id')){
        data[currentTask][event.target.id]=false;
            //if(task[currentTask].shadow) draggable.addClass('shadow');
		    draggable.removeClass('in-the-droppable');
		    draggable.removeClass('draggable-right');
			draggable.removeClass('draggable-wrong');
		    $(event.target).removeClass('withDraggable');
		    $(event.target).removeClass('withDraggableWrong');
            test_drag_n_drop.removeFromGroup(draggable,$(event.target));
       }
   },
   checkSupergroupByAnswerId:function(id,blockNum){
	var input = task[currentTask]['test_drag_n_drop'];
	if(!$.isArray(input))
		input = [input];
	var dnd = input[blockNum];
    if(dnd.correctAnswers[id] && dnd.correctAnswers[id].supergroup){
//        var tmp=0;
        var order=[];
        var groupsCounter = {};
		var answerPositions = [];
        

        for(var i=0;i<dnd.correctAnswers.length;i++){
            if(dnd.correctAnswers[i] && dnd.correctAnswers[i].supergroup){
                if(!groupsCounter[dnd.correctAnswers[i].values]){
                   groupsCounter[dnd.correctAnswers[i].values]=1;
                } else 
                   groupsCounter[dnd.correctAnswers[i].values]++;
                if(dnd.correctAnswersSupergroups[dnd.correctAnswers[i].supergroup].order=='column')
					for(var j=0;j<dnd.correctAnswersSupergroups[dnd.correctAnswers[i].supergroup].groups.length;j++){
						var group=dnd.correctAnswersGroups[dnd.correctAnswersSupergroups[dnd.correctAnswers[i].supergroup].groups[j]];
						for(var k=0;k<group.length;k++){
							if(data[currentTask]['block'+blockNum+'droppable'+i] && dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])] && dnd.answers[test_drag_n_drop.numberFromId(data[currentTask]['block'+blockNum+'droppable'+i])].value==group[k]){
								if(!order[groupsCounter[dnd.correctAnswers[i].values]-1]){
									var dub = false;
									for(var l=0;l<order.length;l++)
										if(order[l]==k) dub = true;
									if(!dub) order[groupsCounter[dnd.correctAnswers[i].values]-1]=k;
								}
								if(id==i && order[groupsCounter[dnd.correctAnswers[i].values]-1]==k){
									return true;
								}
							}
//                       tmp++;
						}
					}
				//end if order = column
				if(dnd.correctAnswersSupergroups[dnd.correctAnswers[i].supergroup].order=='none'){
					answerPositions[i] = [];
					//alert('hz kak obrabatyvat');
					var groupsOrder = [];
					var supergroupGroups = dnd.correctAnswersSupergroups[dnd.correctAnswers[i].supergroup].groups;
					var groupLength = dnd.correctAnswersGroups[supergroupGroups[0]].length;
					for(var j=0;j<supergroupGroups.length;j++){
						//alert();
						
						for(var k=0;k<dnd.correctAnswersGroups[supergroupGroups[j]].length;k++){
							//groupsAnswers[j][dnd.correctAnswersGroups[supergroupGroups[j]][k]] = true;
							//var tmpresult = true;
							
							for(var l=0;l<dnd.correctAnswersGroups[supergroupGroups[j]][k].length;l++){
								//alert(dnd.correctAnswersGroups[supergroupGroups[j]][k]);
							}
						}
					}
					//что дальше делать - хз
					//alert(JSON.stringify({}));
					return true;
				}
            }
        }
        return false;
    } else 
        return true;
   },
    droppableToPlaces:function(){
        for(var i=0; i<task[currentTask]['test_drag_n_drop'].correctAnswers.length;i++){
            //if(data[currentLevel])
        }
    },
    handleStopDraggingEvent:function(event,ui){
        var draggable = $(event.target);
        if(!draggable.hasClass('in-the-droppable')){
            if(draggable.data('stopdragging')=='defaultplace'){
                draggable.animate({"top":$('#block'+test_drag_n_drop.numberFromId(event.target.id,true)+'defaultPlace'+test_drag_n_drop.numberFromId(event.target.id)).css('top'),"left":$('#block'+test_drag_n_drop.numberFromId(event.target.id,true)+'defaultPlace'+test_drag_n_drop.numberFromId(event.target.id)).css('left')})
            }
        }
        
    },
    addDraggableDuplicate:function(i,blockNum){
		var input1 = task[currentTask]['test_drag_n_drop'];
		if(!$.isArray(input1))
			input1 = [input1];
		var dnd = input1[blockNum];
        if(!test_drag_n_drop.defaultplaces || !test_drag_n_drop.defaultplaces["block"+blockNum+"defaultPlace"+i]){
            if(!test_drag_n_drop.defaultplaces) test_drag_n_drop.defaultplaces={};
            draggableDiv = $('<div id="block'+blockNum+'draggable'+i+'duplicate'+new Date().getTime()/1+'"></div>');
            //draggableDiv.id = "draggable"+i+'duplicate'+new Date().getTime()/1;
            test_drag_n_drop.defaultplaces["block"+blockNum+"defaultPlace"+i] = draggableDiv.attr('id');
            //test_drag_n_drop.defaultplaces[event.target.id]
            if(dnd.answerParams.style)
                draggableDiv.css(dnd.answerParams.style);
            if(dnd.answers[i].style)
                draggableDiv.css(dnd.answers[i].style);
            draggableDiv.addClass("draggable");
            if(dnd.answerParams.positions)
                for(key in dnd.answerParams.positions)
                    setSize(draggableDiv,key,dnd.answerParams.positions[key]);
            if(dnd.answers[i].positions)
                for(key in dnd.answers[i].positions)
                    setSize(draggableDiv,key,dnd.answers[i].positions[key]);
            var element = $('<div>'+dnd.answers[i].value+'</div>');
            element.css('width',draggableDiv.css('width'));
            element.css('height',draggableDiv.css('height'));
            if(dnd.answerParams.textOffset)
                element.css('top',dnd.answerParams.textOffset+'px');
            if(dnd.answerParams.textShow && dnd.answerParams.textShow=="removeOnTarget")
                element.addClass('answerText1');
            draggableDiv.append(element);
            getContainer().append(draggableDiv);
            draggableDiv.draggable();
            draggableDiv.mousedown(function(){$(this).addClass('active');});
            draggableDiv.mouseup(function(){
                $(this).removeClass('active');
                setTimeout(function(a){
                    if(!a.hasClass('in-the-droppable') && test_drag_n_drop.defaultplaces["defaultPlace"+test_drag_n_drop.numberFromId(a[0].id)]!=a[0].id){
                        a.remove();
                    }
                },100, $(this));
            });
        }
    }
}