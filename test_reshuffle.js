var test_reshuffle = {

    create: function(input)
    {
        if (!data[currentTask])
            data[currentTask] = {};

        var result = [];
        for (var i = 0; i < input.length; i++)
        {
            result.push(test_reshuffle.addReshuffleRow(input[i], i));
        }
        return $(result);
    },
    check: function()
    {
        var result = true;
        var input = task[currentTask]['test_reshuffle'];
        if (input)
        {
            for (var i = 0; i < input.length; i++)
            {
                if (!test_reshuffle.checkReshuffleRow(input[i], i))
                {
                    result = false;
                }
            }
        }
        return { "result": result, "score": 0 };
    },

    checkReshuffleRow: function(dataObj, index)
    {
        var result = true;
        var jObject = $('#test_reshuffle_row_' + index);
        var reshuffleRow = jObject[0];
        var data = $(reshuffleRow).data('model');

        var arr = new Array();
        for(var i = 0; i < reshuffleRow.children.length; i++)
        {
            arr.push($(reshuffleRow.children[i]));
        }
        arr.sort(data.orientation == "horizontal" ? test_reshuffle.sortDivsByLeftPos : test_reshuffle.sortDivsByTopPos);
        for (var i = 0; i < reshuffleRow.children.length; i++)
        {
            result = false;
            if (dataObj.type == "image")
            {
                for(var j = 0; j < data.correct_orders.length;  j++)
                {
                    if(reshuffleRow.children[data.correct_orders[j][i]] == arr[i].context)
                    {
                        result = true;
                        continue;
                    }
                }
            }
            else
            {
                for (var j = 0; j < data.correct_orders.length; j++)
                {
                    if ($(reshuffleRow.children[data.correct_orders[j][i]]).text() == arr[i].text()) {
                        result = true;
                        continue;
                    }
                }
            }
            if (!result)
                break;
        }



        if (result) {
            if(showCheckAnimation()) jObject.addClass("correct");
        }
        else {
            if (!task[currentTask].check.autoCheck) if(showCheckAnimation()) jObject.addClass("error");
        }

        setTimeout(function (a) {
            a.removeClass("correct");
            a.removeClass("error");

        }, 1100, jObject);
        return result;
    },

    addReshuffleRow: function (reshuffleData, index)
    {
        if (reshuffleData.orientation === undefined)
            reshuffleData.orientation = "horizontal";
        if (reshuffleData.correct_orders === undefined)
            reshuffleData.correct_orders = new Array();
        if (reshuffleData.interval === undefined)
            reshuffleData.interval = 0;
        var correctOrder = new Array();
        var div = document.createElement("div");
        div.id = 'test_reshuffle_row_' + index;
        var item = $(div);
        if (reshuffleData.hint)
            item.data('hint', reshuffleData.hint);
        item.data('model', reshuffleData);
        item.addClass("reshuffleRow");
        if (reshuffleData.position)
        {
            for (key in reshuffleData.position) {
                setSize(item, key, reshuffleData.position[key]);
            }
            var arr = new Array();
            for (var i = 0; i < reshuffleData.values.length; i++)
            {
                correctOrder.push(i);
                var itemDiv = document.createElement("div");
                itemDiv.id = div.id + "_" + i;
                var jitem = $(itemDiv);
                jitem.addClass("draggable");
                jitem.css("position", "absolute");
                if (reshuffleData.style) {
                    jitem.css(reshuffleData.style);
                }
                jitem.draggable({
                    drag: function (e) {
                        test_reshuffle.onLabelDrag(e.target);
                    },
                    stop: function (e) {
                        test_reshuffle.onLabelDragCompleted(e.target.parentNode);
                    }
                });
            
                if (reshuffleData.orientation == "horizontal" && reshuffleData.position["height"])
                    setSize(jitem, "height", reshuffleData.position["height"] - 4);
                else if (reshuffleData.orientation == "vertical" && reshuffleData.position["width"])
                    setSize(jitem, "width", reshuffleData.position["width"] - 4);

                if (reshuffleData.type == "image")
                {
                    jitem.addClass("imageContainer")
                    jitem.html('<image src = "' + reshuffleData.values[i] + '" />');
                } 
                else
                {
                    jitem.text(reshuffleData.values[i]);
                }
                div.appendChild(itemDiv);
                arr.push(i);
            }

            reshuffleData.correct_orders.push(correctOrder);

            if (data[currentTask] && data[currentTask][div.id])
            {
                var stateArr = data[currentTask][div.id];
                for (var i = 0; i < reshuffleData.values.length; i++)
                {
                    arr[i] = stateArr[i].substr(div.id.length + 1);
                }
            }
            else
            {
                test_reshuffle.shuffle(arr);
            }

            for (var i = 0; i < reshuffleData.values.length; i++) {
                test_reshuffle.setShuffleLabelSizeByIndex(div, i, arr[i]);
            }

        }
        return div;
    },
    shuffle: function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

    return array;
    },

    setShuffleLabelSize: function (label, pos) 
    {
        var reshuffleRow = label.parentNode;
        var data = $(reshuffleRow).data('model');
        var jitem = $(label);
        var interval = getSize(data.interval);
        if (data.orientation == "horizontal")
        {
            var rowWidth = parseInt($(reshuffleRow).css("width")) - 2;
            var maxWidth = (rowWidth - (data.values.length - 1) * interval) / reshuffleRow.children.length;
            jitem.css("width", maxWidth - 2);
            jitem.css("left", pos * (maxWidth + interval));
            jitem.css("top", "0px");
        }
        else
        {
            var rowHeight = parseInt($(reshuffleRow).css("height")) - 2;
            var maxHeight = (rowHeight - (data.values.length - 1) * interval) / reshuffleRow.children.length;
            jitem.css("height", maxHeight - 2);
            jitem.css("top", pos * (maxHeight + interval));
            jitem.css("left", "0px");
        }
    },

    setShuffleLabelSizeByIndex: function (reshuffleRow, index, pos)
    {
        var label = reshuffleRow.children[index];
        test_reshuffle.setShuffleLabelSize(label, pos);
    },

    onLabelDrag: function (labelDiv)
    {
        var reshuffleRow = labelDiv.parentNode;
        var data = $(reshuffleRow).data('model');
        var arr = new Array();

        for (var i = 0; i < reshuffleRow.children.length; i++) {
            if (reshuffleRow.children[i] != labelDiv)
                arr.push($(reshuffleRow.children[i]));
        }
        arr.sort(data.orientation == "horizontal" ? test_reshuffle.sortDivsByLeftPos : test_reshuffle.sortDivsByTopPos);
        var jDiv = $(labelDiv);

        var cssAttrName = data.orientation == "horizontal" ? "left" : "top";
        var cssAttrLengthtName = data.orientation == "horizontal" ? "width" : "height";

        var divCenter = parseInt(jDiv.css(cssAttrName)) + parseInt(jDiv.css(cssAttrLengthtName)) / 2;
        var rowWidth = parseInt($(reshuffleRow).css(cssAttrLengthtName));
        var cellWidth = rowWidth / reshuffleRow.children.length;
        var itemPos = Math.floor(divCenter / cellWidth);
        if (itemPos < 0) itemPos = 0;
        if (itemPos > reshuffleRow.children.length - 1)
            itemPos = reshuffleRow.children.length - 1;
        arr.splice(itemPos, 0, null)
        for (var i = 0; i < arr.length; i++)
        {
            if (arr[i])
            {
                test_reshuffle.setShuffleLabelSize(arr[i].context, i);
            }
        }
    },

    onLabelDragCompleted: function (reshuffleRow)
    {
        var arr = new Array();
        var stateArr = new Array();
        var data = $(reshuffleRow).data('model');

        for(var i = 0; i < reshuffleRow.children.length; i++)
        {
            arr.push($(reshuffleRow.children[i]));
        }
        arr.sort(data.orientation == "horizontal" ? test_reshuffle.sortDivsByLeftPos : test_reshuffle.sortDivsByTopPos);
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i].context;
            test_reshuffle.setShuffleLabelSize(item, i);
            stateArr.push(item.id);
        }
        if (task[currentTask].autoCheck)
            check();

        if (!data[currentTask])
            data[currentTask] = {};
        data[currentTask][reshuffleRow.id] = stateArr;
    },
    sortDivsByTopPos: function(a, b)
    {
        var topA = parseInt(a.css("top"));
        var topB = parseInt(b.css("top"));
        return topA == topB ? 0 : (topA > topB ? 1 : -1);
    },
    sortDivsByLeftPos: function (a, b)
    {
        var leftA = parseInt(a.css("left"));
        var leftB = parseInt(b.css("left"));
        return leftA == leftB ? 0 : (leftA > leftB ? 1 : -1);
    }
}