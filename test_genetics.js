var test_genetics = {
	create:function(input){
		var result = $('<div id="genetics"></div>');
		if(input.positions)
			for(var key in input.positions)
				setSize(result, key, input.positions[key]);
			var cell = $('<div class="genetics-figure">'+input.DNA.charAt(0)+'&apos;</div>');
			setSize(cell, 'width',20);
			setSize(cell, 'height',20);
			setSize(cell,'left',8);
			setSize(cell,'font-size',18);
			setSize(cell,'top',8);
			result.append(cell);
			if(input.answer.DNA){
				var cell=$('<div class="genetics-dna-figure" id="dna0"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell,'left',8);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',63);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',73);
				setSize(cell,'left',31);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			if(input.answer.RNA){
				var cell=$('<div class="genetics-dna-figure" id="rna0"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell,'left',8);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',133);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',144);
				setSize(cell,'left',31);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			if(input.answer.peptide){
				var cell=$('<div class="genetics-dna-figure" id="peptide0"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell,'left',8);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',203);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',214);
				setSize(cell,'left',31);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			for(var i=1;i<input.DNA.length-1;i++){
				var cell = $('<div class="genetics-letter">'+input.DNA.charAt(i)+'</div>');
				setSize(cell, 'width',30);
				setSize(cell, 'height',30);
				setSize(cell,'font-size',27);
				setSize(cell, 'left',i*50);
				
				result.append(cell);
				if(input.answer.DNA){
					var cell=$('<div class="genetics-dna-letter" id="dna'+i+'"><div class="genetics-editable" contenteditable="true" id="dnaContent'+i+'"></div></div>');
					setSize(cell, 'width',30);
					setSize(cell, 'height',30);
					setSize(cell.children(), 'width',30);
					setSize(cell.children(), 'height',30);
					cell.children()[0].addEventListener("input", test_genetics.input);
					if(data[currentTask] && data[currentTask][cell.children().attr('id')])
						cell.children().html(data[currentTask][cell.children().attr('id')]);
					setSize(cell,'left',i*50);
					setSize(cell,'font-size',27);
					setSize(cell, 'border-radius', 5);
					setSize(cell,'top',58);
					setSize(cell,'border-width',2,true);
					result.append(cell);
					cell = $('<div class="genetics-vertical-line"></div>');
					setSize(cell,'top',38);
					setSize(cell,'left',(i*50)+16);
					setSize(cell,'height',20);
					setSize(cell,'border-width',2,true);
					result.append(cell);
				}
				if(input.answer.RNA){
					var cell=$('<div class="genetics-dna-letter" id="rna'+i+'"><div class="genetics-editable" contenteditable="true" id="rnaContent'+i+'"></div></div>');
					setSize(cell, 'width',30);
					setSize(cell, 'height',30);
					setSize(cell.children(), 'width',30);
					setSize(cell.children(), 'height',30);
					cell.children()[0].addEventListener("input", test_genetics.input);
					if(data[currentTask] && data[currentTask][cell.children().attr('id')])
						cell.children().html(data[currentTask][cell.children().attr('id')]);
					setSize(cell,'left',i*50);
					setSize(cell,'font-size',27);
					setSize(cell, 'border-radius', 5);
					setSize(cell,'top',128);
					setSize(cell,'border-width',2,true);
					result.append(cell);
					cell = $('<div class="genetics-vertical-line"></div>');
					setSize(cell,'top',108);
					setSize(cell,'left',(i*50)+16);
					setSize(cell,'height',20);
					setSize(cell,'border-width',2,true);
					result.append(cell);
				}
				if(input.answer.peptide){
					var cell=$('<div class="genetics-dna-letter" id="peptide'+i+'"><div class="genetics-editable" contenteditable="true" id="peptideContent'+i+'"></div></div>');
					setSize(cell, 'width',30);
					setSize(cell, 'height',30);
					setSize(cell.children(), 'width',30);
					setSize(cell.children(), 'height',30);
					cell.children()[0].addEventListener("input", test_genetics.input);
					if(data[currentTask] && data[currentTask][cell.children().attr('id')])
						cell.children().html(data[currentTask][cell.children().attr('id')]);
					setSize(cell,'left',i*50);
					setSize(cell,'font-size',27);
					setSize(cell, 'border-radius', 5);
					setSize(cell,'top',198);
					setSize(cell,'border-width',2,true);
					result.append(cell);
				}
			}
			
			cell=$('<div class="genetics-figure">'+input.DNA.charAt(input.DNA.length-1)+'&apos;</div>')
			setSize(cell, 'width',30);
			setSize(cell, 'height',30);
			setSize(cell,'font-size',18);
			setSize(cell,'top',8);
			setSize(cell, 'left',(input.DNA.length-1)*50);
			result.append(cell);
			if(input.answer.DNA){
				var cell=$('<div class="genetics-dna-figure" id="dna'+(input.DNA.length-1)+'"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell, 'left',(input.DNA.length-1)*50+1);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',63);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',73);
				setSize(cell,'left',((input.DNA.length-2)*50)+33);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			if(input.answer.RNA){
				var cell=$('<div class="genetics-dna-figure" id="rna'+(input.DNA.length-1)+'"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell, 'left',(input.DNA.length-1)*50+1);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',133);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',144);
				setSize(cell,'left',((input.DNA.length-2)*50)+33);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			if(input.answer.peptide){
				var cell=$('<div class="genetics-dna-figure" id="peptide'+(input.DNA.length-1)+'"></div>');
				setSize(cell, 'width',20);
				setSize(cell, 'height',20);
				setSize(cell, 'left',(input.DNA.length-1)*50+1);
				setSize(cell,'font-size',18);
				setSize(cell, 'border-radius', 5);
				setSize(cell,'top',203);
				setSize(cell,'border-width',2,true);
				if(data[currentTask] && data[currentTask][cell.attr('id')]){
					cell.data('value', data[currentTask][cell.attr('id')]);
					var draggable = test_genetics.addDraggable(data[currentTask][cell.attr('id')]);
					draggable.addClass('genetics-in-the-droppable');
					draggable.css('top', $(cell).css('top'));
					draggable.css('left', $(cell).css('left'));
					result.append(draggable);
				}
				cell.droppable({
					accept: ".genetics-draggable",
					drop:test_genetics.drop,
					out:test_genetics.out
				});
				result.append(cell);
				cell = $('<div class="genetics-horizontal-line"></div>');
				setSize(cell,'top',214);
				setSize(cell,'left',((input.DNA.length-2)*50)+33);
				setSize(cell,'width',20);
				setSize(cell,'border-width',2,true);
				result.append(cell);
			}
			var draggable=$('<div class="genetics-draggable" data-value="3"><div>3&apos;</div></div>');
			setSize(draggable,'width',20);
			setSize(draggable,'height',20);
			setSize(draggable.children(),'width',20);
			setSize(draggable.children(),'height',20);
			setSize(draggable,'font-size',18);
			setSize(draggable, 'border-radius', 5);
			setSize(draggable,'border-width',2,true);
			result.append(draggable);
			draggable=$('<div class="genetics-draggable" data-value="5"><div>5&apos;</div></div>');
			setSize(draggable,'width',20);
			setSize(draggable,'height',20);
			setSize(draggable.children(),'width',20);
			setSize(draggable.children(),'height',20);
			setSize(draggable,'left',40);
			setSize(draggable,'font-size',18);
			setSize(draggable, 'border-radius', 5);
			setSize(draggable,'border-width',2,true);
			result.append(draggable);
			setTimeout(function(){
				$('.genetics-draggable').draggable({
					stop:test_genetics.stopdragging
				});
			},100);
		window.onkeyup = function(e){return false}
		window.onkeydown = function(e){
			//e.preventDefault();
			var keycode, keyChar;
			if(!e) var e = window.event;
			if (e.keyCode) keycode = e.keyCode;
			else if(e.which) keycode = e.which;
			if(keycode==8){
				
				if($(':focus')[0] && $(':focus').html().length==0){
					var namearray=$(':focus')[0].id.split('Content');
					if(parseInt(namearray[1])==0){
						
					} else {
						var prev = $('#'+namearray[0]+'Content'+(parseInt(namearray[1])-1));
						prev.focus();
						prev.caret(prev.html().length);
					}
				}
					
			}
		}
		return result;
	},
	addDraggable:function(v,append){
		var draggable=$('<div class="genetics-draggable" data-value="'+v+'"><div>'+v+'&apos;</div></div>');
		setSize(draggable,'width',20);
		setSize(draggable,'height',20);
		setSize(draggable.children(),'width',20);
		setSize(draggable.children(),'height',20);
		if(v==5)
			setSize(draggable,'left',40);
		setSize(draggable,'font-size',18);
		setSize(draggable, 'border-radius', 5);
		setSize(draggable,'border-width',2,true);
		if(append)
			$('#genetics').append(draggable);
		setTimeout(function(a){
			a.draggable({
				stop:test_genetics.stopdragging
			});
		},100,draggable);
		return draggable
	},
	stopdragging:function(event,ui){
		var draggable=test_genetics.addDraggable($(event.target).data('value'),true);
		if(!$(event.target).hasClass("genetics-in-the-droppable"))
			$(event.target).remove();
	},
	drop:function(event, ui){
		var draggable = ui.draggable;
		$('.genetics-in-the-droppable').each(function(){
			if($(this).css('top') == $(event.target).css('top') && $(this).css('left') == $(event.target).css('left'))
				this.remove();
		});
		draggable.addClass('genetics-in-the-droppable');
		draggable.css('top', $(event.target).css('top'));
		draggable.css('left', $(event.target).css('left'));
		$(event.target).data('value', draggable.data('value'));
		if(!data[currentTask]) data[currentTask] = {};

		data[currentTask][event.target.id]=draggable.data('value');

		
	},
	out:function(event,ui){
		var draggable = ui.draggable;
		draggable.removeClass('genetics-in-the-droppable');
		if(draggable.data('value')==$(event.target).data('value')){
			$(event.target).data('value','');
			data[currentTask][event.target.id]=false;
		}
			
	},
	input:function() {
		$(this).parent().removeClass('genetics-wrong');
		$(this).parent().removeClass('genetics-right');
		var namearray=this.id.split('Content');
		var next = $('#'+namearray[0]+'Content'+(parseInt(namearray[1])+1));
		if($(this).html().length>0 && next[0])
			next.focus();
		else
			if($(this).html().length>1){
				$(this).html($(this).html().charAt(0));
				$(this).caret($(this).html().length);
			}
		if(!data[currentTask]) data[currentTask] = {};
		data[currentTask][this.id] = $(this).html();
	},
	check:function(){
		var result = true;
		for(var i=0;i<task[currentTask]['test_genetics'].DNA.length;i++){
			if(task[currentTask]['test_genetics'].answer.DNA){
				if(i==0 || i==task[currentTask]['test_genetics'].DNA.length-1){
					if($('#dna'+i).data('value')!=task[currentTask]['test_genetics'].answer.DNA.charAt(i)){
						result = false;
						$('#dna'+i).addClass('genetics-wrong');
					} else {
						$('#dna'+i).addClass('genetics-right');
					}
						
				} else {
					if($('#dnaContent'+i).html()!=task[currentTask]['test_genetics'].answer.DNA.charAt(i)){
						result = false;
						$('#dna'+i).addClass('genetics-wrong');
					} else {
						$('#dna'+i).addClass('genetics-right');
					}
						
				}
			}
			if(task[currentTask]['test_genetics'].answer.RNA){
				if(i==0 || i==task[currentTask]['test_genetics'].DNA.length-1){
					if($('#rna'+i).data('value')!=task[currentTask]['test_genetics'].answer.RNA.charAt(i)){
						result = false;
						$('#rna'+i).addClass('genetics-wrong');
					} else {
						$('#rna'+i).addClass('genetics-right');
					}
						
				} else {
					if($('#rnaContent'+i).html()!=task[currentTask]['test_genetics'].answer.RNA.charAt(i)){
						result = false;
						$('#rna'+i).addClass('genetics-wrong');
					} else {
						$('#rna'+i).addClass('genetics-right');
					}
						
				}
			}
			if(task[currentTask]['test_genetics'].answer.peptide){
				if(i==0 || i==task[currentTask]['test_genetics'].DNA.length-1){
					if($('#peptide'+i).data('value')!=task[currentTask]['test_genetics'].answer.peptide.charAt(i)){
						result = false;
						$('#peptide'+i).addClass('genetics-wrong');
					} else {
						$('#peptide'+i).addClass('genetics-right');
					}
						
				} else {
					if($('#peptideContent'+i).html()!=task[currentTask]['test_genetics'].answer.peptide.charAt(i)){
						result = false;
						$('#peptide'+i).addClass('genetics-wrong');
					} else {
						$('#peptide'+i).addClass('genetics-right');
					}
						
				}
			}
		}
		return {"result":result,"score":0};
	}
};