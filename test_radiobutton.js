var test_radiobutton = {
    create:function(input){
        var result = [];
        for(var i=0;i<input.length;i++){
            /*var checkbox = $('<input type="radio" class="radiobutton" id="radiobutton'+i+'" />');
            if(data[currentTask] && data[currentTask]['radiobutton'+i])
                checkbox.attr('checked','checked');
            if(input[i].hint)
                checkbox.data('hint',input[i].hint);
            if (input[i].style){
                checkbox.css(input[i].style);
            }
            if(input[i].group)
                checkbox.attr('name', input[i].group);
            else
                checkbox.attr('name', 'default');
            if (input[i].positions) {
                for (key in input[i].positions){
                    setSize(checkbox, key, input[i].positions[key]);
                }
            }*/
            if(input[i].text){
                var element = $('<div class="radiobutton"><input type="radio" class="magic-radio" id="radiobutton'+i+'" /><label for="radiobutton'+i+'">'+input[i].text+'</label></div>');
                var checkbox = element.children('input');
            } else {
                var element = $('<div class="radiobutton"><input type="radio" class="magic-radio" id="radiobutton'+i+'" /></div>');
                var checkbox = element.children('input');
            }

            if(data[currentTask] && data[currentTask]['radiobutton'+i])
                checkbox.attr('checked','checked');
            if(input[i].hint)
                checkbox.data('hint',input[i].hint);
            if (input[i].style){
                checkbox.css(input[i].style);
            }
            if(input[i].group)
                checkbox.attr('name', input[i].group);
            else
                checkbox.attr('name', 'default');
            if (input[i].positions) {
                for (key in input[i].positions){
                    if(key!='width')
                        setSize(element, key, input[i].positions[key]);
                    else
                        setSize(checkbox, key, input[i].positions[key]);
                    if(key=='height'){
                        setSize(checkbox, key, input[i].positions[key]);
                        setSize(element,'font-size',input[i].positions[key]*0.8);
                    }
                }
            }



            checkbox.change(function() {
				$('input[name='+this.name+']').parent().find('label').removeClass('correct');
				$('input[name='+this.name+']').parent().find('label').removeClass('error');
                if(!data[currentTask]) data[currentTask]={};
                data[currentTask][this.id]=this.checked;
                if(task[currentTask].check.autoCheck){
                   lastElementId = checkbox.attr('id');
                   check();
                }
            });
            //$('[type=checkbox]').checkbox();
            result.push(element);//checkbox
            //if(input[i].text)
               // result.push($('<label for="radiobutton'+i+'" class="label" style="top:'+checkbox.css('top')+';left:'+(parseInt(checkbox.css('left'))+parseInt(checkbox.css('width')))+'px;height:'+checkbox.css('height')+'">'+input[i].text+'</label>'));
        }
        return $(result);
    },
    check:function(){
        var result = true;
        if(task[currentTask]['test_radiobutton']){
            for(var i=0;i<task[currentTask]['test_radiobutton'].length;i++){
                if(($('#radiobutton'+i)[0].checked && !task[currentTask]['test_radiobutton'][i].right)  || (!$('#radiobutton'+i)[0].checked && task[currentTask]['test_radiobutton'][i].right)){
                    result = false;
                    if($('#radiobutton'+i)[0].checked && /**/showCheckAnimation() && (!lastElementId || lastElementId=='radiobutton'+i)){
                       var groupName = $('#radiobutton'+i)[0].name;
                       $('#radiobutton'+i/*'input[name='+groupName+']'*/).parent().find('label').addClass('error');
                    }//$('#radiobutton'+i).addClass('error');
                } else {
                    if($('#radiobutton'+i)[0].checked && /**/showCheckAnimation() && (!lastElementId || lastElementId=='radiobutton'+i)){
                       var groupName = $('#radiobutton'+i)[0].name;
                       $('#radiobutton'+i/*'input[name='+groupName+']'*/).parent().find('label').addClass('correct');
                    }//$('#radiobutton'+i).addClass('correct');
                }
                /*setTimeout(function (a) {
                    a.removeClass("correct");
                    a.removeClass("error");
                }, 1100, $('#radiobutton'+i));*/
            }
        }
        return {"result":result,"score":0};
    }
}