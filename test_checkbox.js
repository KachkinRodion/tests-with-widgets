var test_checkbox = {
    create:function(input){
        var result = [];
        for(var i=0;i<input.length;i++){
            //var checkbox = $('<input type="checkbox" class="checkbox" id="checkbox'+i+'" />');
            if(input[i].text){
                var element = $('<div class="checkbox"><input type="checkbox" class="magic-checkbox" id="checkbox'+i+'" /><label for="checkbox'+i+'">'+input[i].text+'</label></div>');
                var checkbox = element.children('input');
            } else {
                var element = $('<div class="checkbox"><input type="checkbox" class="magic-checkbox" id="checkbox'+i+'" /></div>');
                var checkbox = element;
            }
            if(data[currentTask] && data[currentTask]['checkbox'+i])
                checkbox.attr('checked','true');
            if(input[i].hint)
                checkbox.data('hint',input[i].hint);
            if (input[i].style){
                checkbox.css(input[i].style);
            }
            if (input[i].positions) {
                for (key in input[i].positions){
                    if(key!='width')
                        setSize(element, key, input[i].positions[key]);
                    else
                        setSize(checkbox, key, input[i].positions[key]);
                    if(key=='height'){
                        setSize(checkbox, key, input[i].positions[key]);
                        setSize(element,'font-size',input[i].positions[key]*0.8);
                    }
                }
            }
            checkbox.change(function() {
				$(this).parent().find('label').removeClass('error');
				$(this).parent().find('label').removeClass('correct');
                if(!data[currentTask]) data[currentTask]={}; 
                data[currentTask][this.id]=this.checked;
                if(task[currentTask].check.autoCheck){
                   lastElementId = checkbox.attr('id');
                   check();
                }
            });
            result.push(element);
            //if(input[i].text)
            //    result.push($('<label for="radiobutton'+i+'" class="label" style="top:'+checkbox.css('top')+';left:'+(parseInt(checkbox.css('left'))+parseInt(checkbox.css('width')))+'px;">'+input[i].text+'</label>'));
        }
        return $(result);
    },
    check:function(){
        var result = true;
        if(task[currentTask]['test_checkbox']){
            for(var i=0;i<task[currentTask]['test_checkbox'].length;i++){
                if(($('#checkbox'+i)[0].checked && !task[currentTask]['test_checkbox'][i].right)  || (!$('#checkbox'+i)[0].checked && task[currentTask]['test_checkbox'][i].right)){
                    result = false;
                    if(showCheckAnimation() && (!lastElementId || lastElementId=='checkbox'+i)) $('#checkbox'+i).parent().find('label').addClass('error');
                } else {
                    if(showCheckAnimation() && (!lastElementId || lastElementId=='checkbox'+i)) $('#checkbox'+i).parent().find('label').addClass('correct');
                }
                /*setTimeout(function (a) {
                    a.removeClass("correct");
                    a.removeClass("error");
                }, 1100, $('#checkbox'+i));*/
            }
        }
        return {"result":result,"score":0};;
    }
}